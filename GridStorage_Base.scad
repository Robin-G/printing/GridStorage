use <GridStorage_Components.scad>

module Base(
        countX=1, countY=1,
        magnetRadius=0.0, magnetHeight=0.0, integratedMagnet=false,    magnetOffset=4.8,
        gap=0.25, p1=0.7, p2=1.8, p3=2.15,
        gridXYSize=42, gridZSize=7, radius = 4,
        wallThickness=1.2 ) {
    // Calculate coordinates (see drawing)
    x1 = p3;
    x2 = p3 + p1;
    y1 = p1 + p2;
    y2 = p1 + p2 + p3;
    x4 = x2 + sqrt(2) * gap;
    y3 = x4 - x1 - gap;
    x3 = x1 + gap;
    y4 = y3 + p2;
    y5 = y2 + (sqrt(2)-1) * gap;
            
    width = countX * gridXYSize - gap;
    depth = countY * gridXYSize - gap;

    intersection() {   
        GridPattern(countX, countY, gridXYSize, gridXYSize) {
            BaseGridCell(gap, p1, p2, p3, gridXYSize, gridZSize, radius);
        }
        translate([-width/2, -depth/2, 0])
            cube([width, depth, p1+p2+p3]);       
    }
    

    baseHeight = ( magnetRadius > 0.0 && magnetHeight > 0.0) ? 
        integratedMagnet ? 
        magnetHeight + 2*gap + 2*wallThickness
        : magnetHeight + gap + wallThickness
        : wallThickness;
    difference() {
        translate([-width/2, -depth/2, -baseHeight])
            cube([width, depth, baseHeight]);
        if( magnetRadius > 0.0 && magnetHeight > 0.0) {
            offset = gridXYSize - 2*x4 - 2*magnetOffset;
            heightOffset = integratedMagnet ? -wallThickness-magnetHeight-2*gap : -magnetHeight-gap;
            GridPattern(countX, countY, gridXYSize, gridXYSize) {
                GridPattern(2,2,offset,offset) {
                    translate( [0,0,heightOffset] )
                        cylinder(  magnetHeight + 2*gap,
                                    magnetRadius + gap, magnetRadius + gap);
                }
            }
        }        
    }
}

// == Object creation ==

Base(4, 4);
