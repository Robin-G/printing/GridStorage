// == UI Parameters ==

/* [General Settings] */

// What type of object should be created
ObjectType = "Box";         // [Box, Base]

// Width in units (one unit is 42mm)
xUnits = 1; // [1, 2, 3, 4]

// Deoth in units (one unit is 42mm)
yUnits = 1; // [1, 2, 3, 4]

// Height in units (one unit is 7mm)
zUnits = 3; // [1, 2, 3, 6, 12, 24]

// How many subdivision should be created in the width direction
WidthSubDivisions=1;

// How many subdivision should be created in the depth direction
DepthSubDivisions=1;

// Should a radius be created in the front for easier component withdrawal
RemovalRadius=0.0; // [0.0, 10.0, 20.0, 30.0]

/* [Label Settings] */

// Should a shelve for labeling at the top be created
TopLabel = true;

// Size of the label shelve
TopLabelSize = 20.0;

// Should a label indentation be created in the front (for 'Glass' the height should be at least 6 units)
FrontLabel = "None";  // [None, Normal, Glas]

// Width of the front label identation
FrontLabelWidth = 30.0;

// Height of the front label identation
FrontLabelHeight = 10.0;

// Thickness of the paper
PaperThickness = 0.5;

/* [Magnet Settings] */

// Should holes be generated for magnets
Magnets = "None";     // [None, Normal, Integrated]

// The diameter of the magnets which will be used
MagnetDiameter = 5.0;

// The height of the magnets which will be used
MagnetHeight = 2.0;

/* [Material Settings] */

// Minimal wall thickness
WallThickness = 1.2;

// == internal Parameters ==

// Number of fragments
$fn = $preview ? 36 : 72;

// == Includes ==

use <GridStorage_Base.scad>
use <GridStorage_Box.scad>

// == Object creation ==

if( ObjectType == "Box" ) {
    Box(xUnits, yUnits, zUnits,
        labelSize=TopLabel ? TopLabelSize : 0.0,
        xDividerCount=WidthSubDivisions-1,
        yDividerCount=DepthSubDivisions-1,
        frontLabelWidth=FrontLabel == "Normal" ? FrontLabelWidth : 0.0,
        frontLabelHeight=FrontLabel == "Normal" ? FrontLabelHeight : 0.0,
        glassLabelFront=FrontLabel == "Glas",
        paperDepth=PaperThickness,
        bottomFrontRadius=RemovalRadius,
        magnetRadius=(Magnets == "None") ? 0.0 : MagnetDiameter/2,
        magnetHeight=(Magnets == "None") ? 0.0 : MagnetHeight,
        integratedMagnet=(Magnets == "Integrated"),
        wallThickness=WallThickness);
} else if ( ObjectType == "Base" ) {
    Base(xUnits, yUnits,
        magnetRadius=(Magnets == "None") ? 0.0 : MagnetDiameter/2,
        magnetHeight=(Magnets == "None") ? 0.0 : MagnetHeight,
        integratedMagnet=(Magnets == "Integrated"),
        wallThickness=WallThickness);
}
