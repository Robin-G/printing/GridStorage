use <GridStorage_Components.scad>

module Box(
        countX=1, countY=1, countZ=1,
        wallThickness=1.2,
        labelSize=0.0,
        xDividerCount=0, yDividerCount=0,
        bottomFrontRadius=0.0,
        glassWidth=76, glassHeight=26, glassDepth=1.5, paperDepth=1, glassFrame=1.5,
        glassLabelFront=false,
        frontLabelWidth=0.0, frontLabelHeight=0.0,
        magnetRadius=0.0, magnetHeight=0.0, integratedMagnet=false, magnetOffset=4.8,
        filled=false,
        gap=0.25, p1=0.7, p2=1.8, p3=2.15,
        gridXYSize=42, gridZSize=7, radius = 4) {
    // Calculate coordinates (see drawing)
    x1 = p3;
    x2 = p3 + p1;
    y1 = p1 + p2;
    y2 = p1 + p2 + p3;
    x4 = x2 + sqrt(2) * gap;
    y3 = x4 - x1 - gap;
    x3 = x1 + gap;
    y4 = y3 + p2;
    y5 = y2 + (sqrt(2)-1) * gap;
            
    width = countX * gridXYSize - 2*gap;
    depth = countY * gridXYSize - 2*gap;
    height = countZ * gridZSize;
            
    difference() {
        group() {
            baseFillRect=gridXYSize-2*x4;         
            GridPattern(countX, countY, gridXYSize, gridXYSize) {
                BaseCell(gap, p1, p2, p3, gridXYSize, gridZSize, radius);
                difference() {
                    RoundedCube(baseFillRect, baseFillRect, y5, radius-x4);
                    if( magnetRadius > 0.0 && magnetHeight > 0.0) {
                        offset = gridXYSize - 2*x4 - 2*magnetOffset;
                        heightOffset = integratedMagnet ? gap + wallThickness : 0.0;
                        GridPattern(2,2,offset,offset) {
                            translate( [0,0,heightOffset-gap] )
                                cylinder(   magnetHeight + 2*gap,
                                            magnetRadius + gap, magnetRadius + gap);
                        }
                    }
                }
            }
            

            translate([0,0,y5])
            difference() {
                RoundedCube(width, depth, height-y5, radius-gap);
                if( !filled ) {
                    translate([0,0,wallThickness])
                        RoundedCube(   width-2*wallThickness,
                                        depth-2*wallThickness,
                                        2*(height-y5),
                                        radius-gap-wallThickness);
                }
            }
            
            translate([0,0,height])
            ExtrudeCell(countX*gridXYSize,countY*gridXYSize,radius) {
                TopBaseProfile(gap, p1, p2, p3);
            }
            
            xDividerStep=width/(xDividerCount+1);
            for( dividerIndex = [1:1:xDividerCount] ) {
                translate([-wallThickness/2+(dividerIndex-0.5*xDividerCount-0.5)*xDividerStep,
                        -depth/2,y5])
                    cube([wallThickness,depth,height-y5]);
            }
            
            yDividerStep=depth/(yDividerCount+1);
            for( dividerIndex = [1:1:yDividerCount] ) {
                translate([-width/2,
                        -wallThickness/2+(dividerIndex-0.5*yDividerCount-0.5)*yDividerStep,y5])
                    cube([width, wallThickness,height-y5]);
            }
        
            for( dividerIndex = [0:1:yDividerCount] ) {
                
                intersection() {
                    translate([-width/2, -depth/2+dividerIndex*yDividerStep, height-labelSize])
                        difference() {
                            cube([width, labelSize, labelSize]);
                            translate([0,labelSize,-labelSize])
                                rotate(45, [1,0,0])
                                cube([width, sqrt(2)*labelSize, sqrt(2)*labelSize]);
                        }
                    translate([0,0,y5])
                        RoundedCube(width, depth, height-y5, radius-gap);
                }
                
                intersection() {
                    translate([-width/2,
                        depth/2-bottomFrontRadius-wallThickness-dividerIndex*yDividerStep,
                        y5])
                        difference() {
                            cube([width, bottomFrontRadius, bottomFrontRadius]);
                            translate([0, 0, bottomFrontRadius])
                                rotate(90, [0, 1, 0])
                                cylinder(width, bottomFrontRadius, bottomFrontRadius);
                        }
                    RoundedCube(width, depth, height, radius-gap);
                }
            }
            
            if( glassLabelFront ) {
                translate([ -glassWidth/2,
                            depth/2-wallThickness-glassDepth,
                            y5])
                    translate([-wallThickness, -wallThickness-paperDepth, 0])
                    cube([  glassWidth+2*wallThickness,
                            wallThickness*2+glassDepth+paperDepth, 
                            height-y5]);
            }
            if( frontLabelWidth > 0.0 && frontLabelHeight > 0.0 ) {
                translate([-frontLabelWidth/2-wallThickness,
                            depth/2-wallThickness-paperDepth, y5])
                    cube([frontLabelWidth+2*wallThickness,
                        wallThickness+paperDepth, height-y5]);
            }            
        }
        
        if( glassLabelFront ) {
            translate([ -0.5*glassWidth,
                        depth/2-wallThickness-glassDepth,
                        height-glassHeight-wallThickness])
                group() {
                    cube([glassWidth, glassDepth, glassHeight]);
                    translate([glassFrame,-paperDepth,glassFrame])
                        cube([glassWidth-2*glassFrame, paperDepth,
                                glassHeight-glassFrame+height]);
                    translate([glassFrame,glassDepth,glassFrame])
                        cube([glassWidth-2*glassFrame, depth,
                                glassHeight-2*glassFrame]);
                }
        }
        if( frontLabelWidth > 0.0 && frontLabelHeight > 0.0 ) {
            translate([-frontLabelWidth/2, depth/2-paperDepth,
                    height-frontLabelHeight-wallThickness])
                cube([frontLabelWidth, depth, frontLabelHeight]);
        }
    }
}

// == Object creation ==

Box(3,2,6);