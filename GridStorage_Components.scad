// == internal Parameters ==

// Number of fragments
$fn = $preview ? 36 : 72;

// == Building Blocks ==

module Triangle(squareSize, otherTriangle=false, center=true) {
    centerOffset= center ? 0 : squareSize/2;
    translate([centerOffset,centerOffset])
        difference() {
            square(squareSize, true);
            offset=squareSize/2;
            size2=sqrt(2)*squareSize;
            offset2= otherTriangle ? -offset : offset;
            translate([offset2, offset2, 0])
                rotate(45, [0,0,1])
                square(size2, true);
        }
}

module OuterBaseProfile(gap, p1, p2, p3) {
    // Calculate coordinates (see drawing)
    x1 = p3;
    x2 = p3 + p1;
    y1 = p1 + p2;
    y2 = p1 + p2 + p3;
    
    group() {
        square([x1, y1], center=false);
        translate([0, y1, 0])
            Triangle(p3, center=false);
        translate([x1, 0, 0])
            Triangle(p1, center=false);
    }
}

module InnerBaseProfile(gap, p1, p2, p3, gridXYSize, gridZSize) {
    // Calculate coordinates (see drawing)
    x1 = p3;
    x2 = p3 + p1;
    y1 = p1 + p2;
    y2 = p1 + p2 + p3;
    x4 = x2 + sqrt(2) * gap;
    y3 = x4 - x1 - gap;
    x3 = x1 + gap;
    y4 = y3 + p2;
    y5 = y2 + (sqrt(2)-1) * gap;

    group() {
        translate([x3, 0, 0])
            Triangle(y3, otherTriangle=true, center=false);
        translate([x3, y3, 0])
            square([y3, y5-y3], center=false);
        translate([gap, y4, 0])
            Triangle(y5-y4, otherTriangle=true, center=false);
    }
}

module TopBaseProfile(gap, p1, p2, p3) {
    // Calculate coordinates (see drawing)
    x2 = p3 + p1;
    y2 = p1 + p2 + p3;
    
    // Reuse baseprofile ...
    // but remove gap and ...
    // add 45° triangle to prevent 90° overhange
    group() {
        difference() {
            OuterBaseProfile(gap, p1, p2, p3);
            square([gap, y2], center = false);
        }
        translate([gap, 0, 0])
            rotate(-90, [0,0,1])
            Triangle(x2-gap, center=false);
        
    }
}

module ExtrudeCell(width, depth, radius) {
    reducedWidth=width-2*radius;
    reducedDepth=depth-2*radius;
    group() {
        for( angle = [90:180:360]) {
            rotate(angle, [0,0,1])
            translate([-depth/2, reducedWidth/2])
            rotate(90, [1,0,0])
            linear_extrude(reducedWidth)
            for(i=[0:1:$children-1]) {
                children(i);
            }
        }
        for( angle = [0:180:360]) {
            rotate(angle, [0,0,1])
            translate([-width/2, reducedDepth/2])
            rotate(90, [1,0,0])
            linear_extrude(reducedDepth)
            for(i=[0:1:$children-1]) {
                children(i);
            }
        }
        translate([-width/2+radius,-depth/2+radius,0])
            rotate_extrude(angle=90)
            for(i=[0:1:$children-1]) {
                translate([-radius,0,0]) children(i);
            }
        rotate(180, [0,0,1])
            translate([-width/2+radius,-depth/2+radius,0])
            rotate_extrude(angle=90)
            for(i=[0:1:$children-1]) {
                translate([-radius,0,0]) children(i);
            }
        rotate(90, [0,0,1])
            translate([-depth/2+radius,-width/2+radius,0])
            rotate_extrude(angle=90)
            for(i=[0:1:$children-1]) {
                translate([-radius,0,0]) children(i);
            }
        rotate(270, [0,0,1])
            translate([-depth/2+radius,-width/2+radius,0])
            rotate_extrude(angle=90)
            for(i=[0:1:$children-1]) {
                translate([-radius,0,0]) children(i);
            }
    }   
}

module RoundedCube(width, depth, height, radius) {
    reducedWidth = width - 2*radius;
    reducedDepth = depth - 2*radius;
    minkowski() {
        cylinder(height/2, radius, radius);
        translate([-reducedWidth/2,-reducedDepth/2, 0])
            cube([reducedWidth,reducedDepth,height/2]);    }
}

module BaseGridCell(gap, p1, p2, p3, gridXYSize, gridZSize, radius) {
    // Calculate coordinates (see drawing)
    y2 = p1 + p2 + p3;
    
    group() {
        difference() {
            translate([-gridXYSize/2,-gridXYSize/2, 0])
                cube([gridXYSize, gridXYSize, y2]);
            translate([0, 0, -y2])
            RoundedCube(gridXYSize, gridXYSize, y2*3, radius);
        }
        
        ExtrudeCell(gridXYSize,gridXYSize,radius) {
            OuterBaseProfile(gap=0.25, p1=0.7, p2=1.8, p3=2.15);
        }
    }
}

module GridPattern(countX, countY, offsetX, offsetY) {
    group() {
        for( x = [0:1:countX-1] ) {
            for( y = [0:1:countY-1] ) {
                for(i=[0:1:$children-1]) {
                    xf = (x - 0.5 * countX + 0.5) * offsetX;
                    yf = (y - 0.5 * countY + 0.5) * offsetY;
                    translate([xf, yf, 0])
                        children(i);
                }
            }
        }
    }
}

module BaseCell(gap, p1, p2, p3, gridXYSize, gridZSize, radius) {
    // Calculate coordinates (see drawing)
    y2 = p1 + p2 + p3;
 
    ExtrudeCell(gridXYSize,gridXYSize,radius) {
        InnerBaseProfile(gap, p1, p2, p3);
    }
}
